package org.yss.weather.service;

import org.yss.weather.service.impl.WeatherCollectorEndpointRestImpl;
import org.yss.weather.service.impl.WeatherQueryEndpointRestImpl;
import java.io.IOException;
import java.util.Set;
import javax.ws.rs.core.Response;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class WeatherQueryEndpointRestImplTest {

    private WeatherCollectorEndpoint collectorEndpoint;

    @Before
    public void setup() {
        WeatherQueryEndpointRestImpl.init();
        collectorEndpoint = new WeatherCollectorEndpointRestImpl();
    }

    @Test
    public void shouldHasValidDataWhenCorrectFileUpdated() throws IOException, WeatherException {
        collectorEndpoint.addAirport("YSS","0","0");
        Response response = collectorEndpoint.getAirports();
        Set<String> airportList  = (Set<String>)response.getEntity();
        assertThat(airportList.size(),is(equalTo(6)));
        assertThat(airportList.stream().filter(t -> t.equals("YSS")).findFirst().get(),is(equalTo("YSS")) );// isIterableContainingInAnyOrder contains()6,is);Equals(6,..getNumberOfAirportsWithAnyFrequency());
    }

    @Test
    public void shouldReturnToInitialSizeWhenAdditionalDataRemoved() throws IOException, WeatherException {
        collectorEndpoint.addAirport("YSS","0","0");
        collectorEndpoint.deleteAirport("YSS");
        Response response = collectorEndpoint.getAirports();
        Set<String> airportList  = (Set<String>)response.getEntity();
        assertThat(airportList.size(),is(equalTo(5)));
        assertThat(airportList.stream().filter(t -> t.equals("YSS")).count(),is(equalTo(0L)) );
    }
}
