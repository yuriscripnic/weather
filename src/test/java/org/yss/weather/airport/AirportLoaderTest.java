package org.yss.weather.airport;

import org.yss.weather.AirportLoader;
import org.yss.weather.service.WeatherException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Before;
import org.junit.Test;

public class AirportLoaderTest extends JerseyTest {

    private AirportLoader loader;

    @Path("/collect/airport")
    public static class Resource {

        @POST
        @Path("/{iata}/{lat}/{long}")
        public Response post(@PathParam("iata") String iata,
                             @PathParam("lat") String latString,
                             @PathParam("long") String longString) {
            return Response.status(Response.Status.OK).build();
        }
    }

    @Override
    protected Application configure() {
        forceSet(TestProperties.CONTAINER_PORT, "9090");
        return new ResourceConfig(Resource.class);
    }

    @Before
    public void setup() {
        loader = new AirportLoader();
    }

    @Test
    public void shouldSuccessWhenFileIsCorrect() throws IOException, WeatherException {
        try (FileInputStream file = new FileInputStream(new File("src/main/resources/airports.dat"))) {
            loader.upload(file);
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenFileIsWrong() throws IOException, WeatherException {
        try (ByteArrayInputStream input = new ByteArrayInputStream("uuiyiuyuddadadasdsi".getBytes("UTF-8"))) {
            loader.upload(input);
        }
    }


}
