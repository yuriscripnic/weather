package org.yss.weather.service;

/**
 * An internal exception marker
 */
public final class WeatherException extends Exception {
    public WeatherException(String message) {
        super(message);
    }
}
