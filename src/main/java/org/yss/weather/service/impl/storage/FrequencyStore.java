package org.yss.weather.service.impl.storage;

import org.yss.weather.domain.AirportData;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class used to save information about usage frequencies and share between modules.
 * It was made as a singleton and use Concurrent Hash Map to make the data
 * access thread safe.
 *
 * @author Yuri Scripnic
 */

public final class FrequencyStore {
    private static FrequencyStore instance;

    /**
     * Internal performance counter to better understand most requested information,
     * this map can be improved but for now provides the basis for future performance optimizations.
     * Due to the stateless deployment architecture we don't want to write this to disk,
     * but will pull it off using a REST request and aggregate with other
     * performance metrics { #ping()}
     */
    private Map<AirportData, Integer> requestFrequency;

    private Map<Double, Integer> radiusFreq;

    static {
        instance = new FrequencyStore();
    }

    private FrequencyStore() {
        requestFrequency = new ConcurrentHashMap<>(1000);
        radiusFreq = new ConcurrentHashMap<>(1000);
    }

    public static FrequencyStore getInstance() {
        return instance;
    }

    /**
     * Gives the number of requests that  all the airports received
     *

     * @return int with the frequency number
     */
    public int getNumberOfAirportsWithAnyFrequency() {
        return requestFrequency.size();
    }

    public int getAirportFrequency(AirportData airportData) {
        return requestFrequency.getOrDefault(airportData, 0);
    }

    public void addAirportFrequency(AirportData airportData, int value) {
        requestFrequency.put(airportData, value);
    }


    public void addRadiusFrequency(double radius, int freq) {
        radiusFreq.put(radius, freq);
    }

    public int getRadiusFrequency(double radius) {
        return radiusFreq.getOrDefault(radius, 0);
    }


    /**
     * Returns a map with calculation of the percentage of queries that a certain airport received over the
     * total amount, for all airports.
     *
     * @param airports      Airport Storage

     * @return a a Map with Fraction of accesses for each airport
     */
    public Map<String, Double> getFractionOfQueries(AirportStore airports) {
        Map<String, Double> freq = new HashMap<>();
        int size = 1;
        if (getNumberOfAirportsWithAnyFrequency() > 0) {
            size = getNumberOfAirportsWithAnyFrequency();
        }
        // fraction of queries
        for (AirportData data : airports.getAllAirports()) {
            double frac = (double) getAirportFrequency(data) / size;
            freq.put(data.getIata(), frac);
        }
        return freq;
    }

    /**
     * Returns a array of int representing a histogram of Frequency of radius requested.
     *
     * @return a array of int representing a histogram
     */
    public int[] getRadiusFrequencyHistogram() {
        int m = radiusFreq.keySet().stream()
                .max(Double::compare)
                .orElse(1000.0).intValue() + 1;

        int[] hist = new int[m];
        for (Map.Entry<Double, Integer> e : radiusFreq.entrySet()) {
            int i = e.getKey().intValue() % 10;
            hist[i] += e.getValue();
        }
        return hist;
    }


    public void clear() {
        requestFrequency.clear();
        radiusFreq.clear();
    }

}
