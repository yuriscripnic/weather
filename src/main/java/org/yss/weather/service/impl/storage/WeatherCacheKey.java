package org.yss.weather.service.impl.storage;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class used by the cache to identify a server response.
 * It holds the iata code and radius parameters, improving the
 * access speed of queries.
 *
 * @author Yuri Scripnic
 */

public final class WeatherCacheKey implements Serializable {
    private String iata;
    private String radius;

    public WeatherCacheKey(String iata, String radiusString) {
        this.iata = iata;
        this.radius = radiusString;
    }

    public String getIata() {
        return iata;
    }

    public String getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        WeatherCacheKey otherKey = (WeatherCacheKey) o;
        return Objects.equals(iata, otherKey.getIata()) &&
                Objects.equals(radius, otherKey.getRadius());
    }

    @Override
    public int hashCode() {
        return Objects.hash(iata, radius);
    }
}
