package org.yss.weather.service.impl.storage;

import org.yss.weather.domain.AirportData;
import org.yss.weather.domain.AtmosphericInformation;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class used to save the airports information and share between modules.
 * It was made as a singleton and use Concurrent Hash Map to make the data
 * access thread safe.
 *
 * @author Yuri Scripnic
 */
public final class AirportStore {

    private Map<String, AirportData> airports;
    private static AirportStore instance;

    /**
     * earth radius in KM
     */
    public static final double R = 6372.8;

    static {
        instance = new AirportStore();
    }

    private AirportStore() {
        airports = new ConcurrentHashMap<>(1000);
    }

    /**
     * Method that return the singleton instance of the storage
     *
     * @return the instance
     */
    public static AirportStore getInstance() {
        return instance;
    }


    public AirportData get(String iata) {
        return airports.get(iata);
    }

    public void add(AirportData airportData) {
        airports.put(airportData.getIata(), airportData);
    }

    public void remove(String iata) {
        airports.remove(iata);
    }


    /**
     * Get the Atmospheric Information of all airports nearby a certain airport
     *
     * @param iataCode      the 3 letter airport code
     * @param radius         the maximum distance between two airports
     * @return a List of Atmospheric Information of the nearby airports
     */
    public List<AtmosphericInformation> getAtmosphericInformationFromNearByAirports(String iataCode, double radius) {
        List<AtmosphericInformation> retval = new ArrayList<>();
        AirportData ad = airports.get(iataCode);
        if (radius == 0) {
            retval.add(ad.getAtmosphericInformation());
        } else {
            for (AirportData airport : airports.values()) {
                if ((calculateDistance(ad, airport) <= radius)
                        && (airport.getAtmosphericInformation().hasAnyInformation())) {
                    retval.add(airport.getAtmosphericInformation());
                }
            }
        }
        return retval;
    }

    /**
     * Return the number of airports that have valid information and
     * had been updated in last day.
     *
     * @return number of valid airports
     */
    public int getNumberOfAirportsWithValidInformation() {
        int datasize = 0;
        for (AirportData ad : airports.values()) {
            // we only count recent readings
            AtmosphericInformation ai = ad.getAtmosphericInformation();
            if (ai.hasAnyInformation() && ai.isUpdatedInTheLastDay()) {
                datasize++;
            }
        }
        return datasize;
    }

     public void clear() {
        airports.clear();
    }

    /**
     * Return a set of all iata codes of the airports  already stored
     *
     * @return Set of Iata codes
     */
    public Set<String> getAllIataCodes() {
        return airports.keySet();
    }

    public Collection<AirportData> getAllAirports() {
        return airports.values();
    }

    /**
     * Haversine distance between two airports.
     *
     * @param ad1 airport 1
     * @param ad2 airport 2
     * @return the distance in KM
     */
    private double calculateDistance(AirportData ad1, AirportData ad2) {
        double deltaLat = Math.toRadians(ad2.getLatitude() - ad1.getLatitude());
        double deltaLon = Math.toRadians(ad2.getLongitude() - ad1.getLongitude());
        double a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.pow(Math.sin(deltaLon / 2), 2)
                * Math.cos(ad1.getLatitude()) * Math.cos(ad2.getLatitude());
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }


}
