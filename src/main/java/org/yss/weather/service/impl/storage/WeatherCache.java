package org.yss.weather.service.impl.storage;

import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.xml.XmlConfiguration;

import static org.ehcache.config.builders.CacheManagerBuilder.newCacheManager;
/**
 * Class used to cache the response data.
 * It was made as class with only static access and use EH Cache to store the data
 * Because its need to implement the Serializable interface, the cache
 * is using ArrayList implementation instead the of List interface.
 *
 * The cache is updated when a query don't find a iata code , radius combination.
 * When a new airport is added or an airport has your Atmospheric Information
 * updated its remove that airport from the cache, allowing a new data set to be created.
 *
 * @author Yuri Scripnic
 */
public class WeatherCache {
    private static Cache<WeatherCacheKey, ArrayList> cache;
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger();

    static {
        init();
    }

    /**
     * Private constructor because the class is just Static
     *
     */
    private WeatherCache(){}

    /**
     * Initialize the cache, configuring it using the ehcache.xml found on classpath.
     *
     */
    public static void init() {
        log.info("Creating cache manager using ehcache.xml");
        Configuration xmlConfig = new XmlConfiguration(WeatherCache.class.getResource("/ehcache.xml"));
        CacheManager cacheManager = newCacheManager(xmlConfig);
        cacheManager.init();
        cache = cacheManager.getCache("weather-cache", WeatherCacheKey.class, ArrayList.class);
    }

    public static void put(String iata, String radius, ArrayList response) {
        cache.put(new WeatherCacheKey(iata, radius), response);
    }

    public static ArrayList get(String iata, String radius) {
        return cache.get(new WeatherCacheKey(iata, radius));
    }

    /**
     * Removes all cache about a certain airport.
     * This allow that new data about this airport can be calculated.
     *
     * @param iataCode   Code of the airport to be removed.
     */
    public static void remove(String iataCode) {
        Set<WeatherCacheKey> removeSet = new HashSet<>();
        Iterator<Cache.Entry<WeatherCacheKey, ArrayList>> it = cache.iterator();
        while (it.hasNext()) {
            WeatherCacheKey key = it.next().getKey();
            if (key.getIata().equals(iataCode)) {
                removeSet.add(key);
            }
        }
        cache.removeAll(removeSet);
    }
}
