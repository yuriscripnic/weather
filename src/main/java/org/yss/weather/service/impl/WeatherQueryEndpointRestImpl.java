package org.yss.weather.service.impl;

import org.yss.weather.domain.AirportData;
import org.yss.weather.domain.AtmosphericInformation;
import org.yss.weather.service.WeatherQueryEndpoint;
import org.yss.weather.service.impl.storage.AirportStore;
import org.yss.weather.service.impl.storage.FrequencyStore;
import org.yss.weather.service.impl.storage.WeatherCache;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * The Weather App REST endpoint allows clients to query, update and check health stats.
 * Currently, all data is held in memory. The end point deploys to a single container
 *
 * @author code test administrator
 */
@Path("/query")
public class WeatherQueryEndpointRestImpl implements WeatherQueryEndpoint {

    /**
     * shared gson json to object factory
     */
    public static final Gson gson = new Gson();


    /**
     * all known airports
     */
    private AirportStore airports = AirportStore.getInstance();
    private FrequencyStore frequency = FrequencyStore.getInstance();


    static {
        init();
    }

    /**
     * Retrieve service health including total getNumberOfAirportsWithAnyFrequency of valid data points and request
     * frequency information.
     *
     * @return health stats for the service as a string
     */

    @Override
    public String ping() {
        Map<String, Object> retval = new HashMap<>();
        retval.put("datasize", airports.getNumberOfAirportsWithValidInformation());
        retval.put("iata_freq", frequency.getFractionOfQueries(airports));
        retval.put("radius_freq", frequency.getRadiusFrequencyHistogram());
        return gson.toJson(retval);
    }


    /**
     * Given a query in json format {'iata': CODE, 'radius': km} extracts the requested
     * airport information and return a list of matching atmosphere information.
     *
     * @param iata         the iataCode
     * @param radiusString the radius in km
     * @return a list of atmospheric information
     */
    @Override
    public Response weather(String iata, String radiusString) {
        ArrayList cachedResponse = WeatherCache.get(iata, radiusString);
        if (cachedResponse != null) {
            return Response.status(Response.Status.OK).entity(cachedResponse).build();
        }
        double radius = NumberUtils.toDouble(radiusString, 0);

        updateRequestFrequency(iata, radius);

        ArrayList<AtmosphericInformation> retval = (ArrayList<AtmosphericInformation>) airports.getAtmosphericInformationFromNearByAirports(iata, radius);
        WeatherCache.put(iata, radiusString, retval);
        return Response.status(Response.Status.OK).entity(retval).build();
    }

    /**
     * Records information about how often requests are made
     *
     * @param iata   an iata code
     * @param radius query radius
     */
    public void updateRequestFrequency(String iata, Double radius) {
        AirportData airportData = airports.get(iata);
        frequency.addAirportFrequency(airportData, frequency.getAirportFrequency(airportData) + 1);
        frequency.addRadiusFrequency(radius, frequency.getRadiusFrequency(radius));
    }

    /**
     * Static initialization with dummy data
     */

    public static void init() {
        AirportStore airportStoreInstance = AirportStore.getInstance();
        FrequencyStore.getInstance().clear();
        airportStoreInstance.clear();
        airportStoreInstance.add(new AirportData.Builder().withIata("BOS").withLatitude(42.364347).withLongitude(-71.005181).build());
        airportStoreInstance.add(new AirportData.Builder().withIata("EWR").withLatitude(40.6925).withLongitude(-74.168667).build());
        airportStoreInstance.add(new AirportData.Builder().withIata("JFK").withLatitude(40.639751).withLongitude(-73.778925).build());
        airportStoreInstance.add(new AirportData.Builder().withIata("LGA").withLatitude(40.777245).withLongitude(-73.872608).build());
        airportStoreInstance.add(new AirportData.Builder().withIata("MMU").withLatitude(40.79935).withLongitude(-74.4148747).build());
    }

}
