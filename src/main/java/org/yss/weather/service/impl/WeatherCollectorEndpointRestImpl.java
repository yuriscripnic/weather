package org.yss.weather.service.impl;

import org.yss.weather.domain.AirportData;
import org.yss.weather.domain.DataPoint;
import org.yss.weather.domain.DataPointType;
import org.yss.weather.service.WeatherCollectorEndpoint;
import org.yss.weather.service.WeatherException;
import org.yss.weather.service.impl.storage.AirportStore;
import org.yss.weather.service.impl.storage.WeatherCache;
import com.google.gson.Gson;
import java.util.Set;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A REST implementation of the WeatherCollector API. Accessible only to airport weather collection
 * sites via secure VPN.
 *
 * @author code test administrator
 */

@Path("/collect")
public final class WeatherCollectorEndpointRestImpl implements WeatherCollectorEndpoint {
    private static final Logger log = LogManager.getLogger(WeatherCollectorEndpointRestImpl.class);

    /**
     * shared gson json to object factory
     */
    public static final Gson gson = new Gson();

    private AirportStore airports = AirportStore.getInstance();

    @Override
    public Response ping() {
        return Response.status(Response.Status.OK).entity("ready").build();
    }

    @Override
    public Response updateWeather(@PathParam("iata") String iataCode,
                                  @PathParam("pointType") String pointType,
                                  String datapointJson) {
        try {
            WeatherCache.remove(iataCode);
            addDataPoint(iataCode, pointType, gson.fromJson(datapointJson, DataPoint.class));
        } catch (WeatherException e) {
            log.error(e.getMessage());
        }
        WeatherCache.remove(iataCode);
        return Response.status(Response.Status.OK).build();
    }


    @Override
    public Response getAirports() {
        Set<String> retval = airports.getAllIataCodes();
        return Response.status(Response.Status.OK).entity(retval).build();
    }

    @Override
    public Response getAirport(@PathParam("iata") String iata) {
        AirportData ad = airports.get(iata);
        return Response.status(Response.Status.OK).entity(ad).build();
    }

    @Override
    public Response addAirport(@PathParam("iata") String iata,
                               @PathParam("lat") String latString,
                               @PathParam("long") String longString) {
        WeatherCache.remove(iata);
        addAirport(iata, Double.valueOf(latString), Double.valueOf(longString));
        return Response.status(Response.Status.OK).build();
    }


    @Override
    public Response deleteAirport(@PathParam("iata") String iata) {
        airports.remove(iata);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response exit() {
        System.exit(0);
        return Response.noContent().build();
    }
    //
    // Internal support methods
    //

    /**
     * Update the airports weather data with the collected data.
     *
     * @param iataCode  the 3 letter IATA code
     * @param pointType the point type {@link DataPointType}
     * @param dp        a datapoint object holding pointType data
     * @throws WeatherException if the update can not be completed
     */
    public void addDataPoint(String iataCode, String pointType, DataPoint dp)
            throws WeatherException {
        try {
            DataPointType type = DataPointType.valueOf(pointType.toUpperCase());
            dp.setType(type);
            AirportData airportData = airports.get(iataCode);
            airportData.addDataPoint(dp);
        } catch (Exception e) {
            throw new WeatherException("Error ading new DataPoint:" + e.getMessage());
        }
    }

    /**
     * Add a new known airport to our list.
     *
     * @param iataCode  3 letter code
     * @param latitude  in degrees
     * @param longitude in degrees
     * @return the added airport
     */
    public AirportData addAirport(String iataCode, double latitude, double longitude) {
        AirportData airportData = new AirportData.Builder()
                .withIata(iataCode)
                .withLatitude(latitude)
                .withLongitude(longitude)
                .build();
        airports.add(airportData);
        return airportData;
    }
}
