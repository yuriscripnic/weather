package org.yss.weather;

import org.yss.weather.domain.AirportData;
import org.yss.weather.service.WeatherException;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A simple airport loader which reads a file from disk and sends entries to the webservice
 * It accept CSV files in Excel Format.
 *
 * @author code test administrator
 */
public final class AirportLoader {

    private static final Logger log = LogManager.getLogger(AirportLoader.class);

    /**
     * end point for read queries
     */
    private WebTarget query;

    /**
     * end point to supply updates
     */
    private WebTarget collect;

    public enum Headers {
        ID, Name, City, Country, IATA, ICAO, Latitude, Longitude, Altitude, Timezone, DST
    }

    public AirportLoader() {
        Client client = ClientBuilder.newClient();
        query = client.target("http://localhost:9090/query");
        collect = client.target("http://localhost:9090/collect");
    }



    public void upload(InputStream airportDataStream) throws IOException, WeatherException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(airportDataStream,"UTF-8"))) {
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader(Headers.class).parse(reader);
            for (CSVRecord record : records) {
                AirportData airportData = new AirportData.Builder()
                        .withIata(record.get(Headers.IATA))
                        .withLatitude(Double.valueOf(record.get(Headers.Latitude)))
                        .withLongitude(Double.valueOf(record.get(Headers.Longitude)))
                        .build();
                log.info("Airport Data sent to server:" + airportData);
                populate(airportData);
            }
        }

    }


    private void populate(AirportData airportData) throws WeatherException {
        try {
            String iata = new URI(airportData.getIata()).toASCIIString();
            String lat = new URI(Double.toString(airportData.getLatitude())).toASCIIString();
            String lon = new URI(Double.toString(airportData.getLongitude())).toASCIIString();


            Response response = collect.path("/airport/{iata}/{lat}/{long}")
                    .resolveTemplate("iata", iata)
                    .resolveTemplate("lat", lat)
                    .resolveTemplate("long", lon)
                    .request()
                    .post(Entity.json(airportData));
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                log.error("Error sending Airport Data. Response Status:" + response.getStatus());
                throw new WeatherException("Error sending Airport Data: Status:" + response.getStatus());
            }
        } catch (URISyntaxException ex) {
            throw new WeatherException(ex.getMessage());
        }

    }


    public static void main(String[] args) throws IOException, WeatherException {
        File airportDataFile = new File(args[0]);
        if ((!airportDataFile.exists()) || (airportDataFile.length() == 0)) {
            log.error(airportDataFile + " is not a valid input");
            System.exit(1);
        }

        AirportLoader al = new AirportLoader();
        al.upload(new FileInputStream(airportDataFile));
        System.exit(0);
    }
}
