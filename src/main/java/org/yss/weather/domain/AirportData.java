package org.yss.weather.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.*;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
public final class AirportData implements Serializable {

    /**
     * the three letter IATA code
     */
    private String iata;

    /**
     * latitude value in degrees
     */
    private double latitude;

    /**
     * longitude value in degrees
     */
    private double longitude;

    private AtmosphericInformation atmosphericInformation= new AtmosphericInformation();

    private AirportData() {
        //Do nothing
    }

    @JsonIgnore
    public void addDataPoint( DataPoint dp) {
        dp.getType().mapDataPointToAtmInfo(dp,atmosphericInformation);
        atmosphericInformation.setLastUpdateTime(System.currentTimeMillis());
    }

    @JsonIgnore
    public DataPoint getDataPoint(DataPointType dpt) {
        return dpt.getDataPointFromAtmInfo(atmosphericInformation);
    }

    @JsonIgnore
    public void removeDataPoint(DataPointType dpt) {
        dpt.mapDataPointToAtmInfo(null,atmosphericInformation);
    }

    @JsonIgnore
    public AtmosphericInformation getAtmosphericInformation(){
        return atmosphericInformation;
    }


    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AirportData)) {
            return false;
        }
        AirportData that = (AirportData) o;
        return Objects.equals(getIata(), that.getIata());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getIata());
    }

    public static class Builder {

        private AirportData airportData = new AirportData();


        public Builder withIata(String iata) {
            this.airportData.iata = iata;
            return this;
        }


        public Builder withLatitude(double latitude) {
            this.airportData.latitude = latitude;
            return this;
        }

        public Builder withLongitude(double longitude) {
            this.airportData.longitude = longitude;
            return this;
        }

        public AirportData build() {
            Object newAirportData;
            newAirportData = SerializationUtils.clone(airportData);
            airportData = new AirportData();
            return (AirportData) newAirportData;
        }


    }


}
