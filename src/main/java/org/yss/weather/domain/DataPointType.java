package org.yss.weather.domain;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * The various types of data points we can collect.
 * This class contains lambda function for valid and
 * transfer data between AtmosphericInformation and
 * DataPoint classes
 *
 *
 * @author code test administrator
 */
public enum DataPointType {
    WIND(dp -> dp.getMean() >= 0,
            (dataPoint, atmInf) -> atmInf.setWind(dataPoint),
            AtmosphericInformation::getWind
    ),
    TEMPERATURE(dp -> dp.getMean() >= -50 && dp.getMean() < 100,
            (dataPoint, atmInf) -> atmInf.setTemperature(dataPoint),
            AtmosphericInformation::getTemperature
    ),
    HUMIDITY(dp -> dp.getMean() >= 0 && dp.getMean() < 100,
            (dataPoint, atmInf) -> atmInf.setHumidity(dataPoint),
            AtmosphericInformation::getHumidity
    ),
    PRESSURE(dp -> dp.getMean() >= 650 && dp.getMean() < 800,
            (dataPoint, atmInf) -> atmInf.setPressure(dataPoint),
            AtmosphericInformation::getPressure
    ),
    CLOUDCOVER(dp -> dp.getMean() >= 0 && dp.getMean() < 100,
            (dataPoint, atmInf) -> atmInf.setCloudCover(dataPoint),
            AtmosphericInformation::getCloudCover
    ),
    PRECIPITATION(dp -> dp.getMean() >= 0 && dp.getMean() < 100,
            (dataPoint, atmInf) -> atmInf.setPrecipitation(dataPoint),
            AtmosphericInformation::getPrecipitation
    );

    private Predicate<DataPoint> filter;
    private BiConsumer<DataPoint, AtmosphericInformation> mapper;
    private Function<AtmosphericInformation, DataPoint> provider;

    DataPointType(Predicate<DataPoint> filter,
                  BiConsumer<DataPoint, AtmosphericInformation> mapper,
                  Function<AtmosphericInformation, DataPoint> provider) {
        this.filter = filter;
        this.mapper = mapper;
        this.provider = provider;
    }


    public boolean isValid(DataPoint dataPoint) {
        return filter.test(dataPoint);
    }

    public void mapDataPointToAtmInfo(DataPoint dataPoint, AtmosphericInformation atmInfo) {
        mapper.accept(dataPoint, atmInfo);
    }

    public DataPoint getDataPointFromAtmInfo(AtmosphericInformation atmInfo) {
        return provider.apply(atmInfo);
    }
}
