package org.yss.weather.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A collected point, including some information about the range of collected values
 *
 * @author code test administrator
 */
public final class DataPoint implements Serializable{

    private double mean = 0.0D;

    private int first = 0;

    private int median = 0;

    private int last = 0;

    private int count = 0;

    @JsonIgnore
    @ToStringExclude
    private DataPointType type;

    /**
     * private constructor, use the builder to create this object
     */
    private DataPoint() {
    }


     protected DataPoint(DataPointType type,int first, int median, int mean, int last, int count) {
        this.setType(type);
        this.setFirst(first);
        this.setMean(mean);
        this.setMedian(median);
        this.setLast(last);
        this.setCount(count);
    }

    public DataPointType getType() {
        return type;
    }

    public void setType(DataPointType type) {
        this.type = type;
    }


    /**
     * the mean of the observations
     */
    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    /**
     * 1st quartile -- useful as a lower bound
     */
    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    /**
     * 2nd quartile -- median value
     */
    public int getMedian() {
        return median;
    }

    public void setMedian(int median) {
        this.median = median;
    }

    /**
     * 3rd quartile value or last value -- less noisy upper value
     */
    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    /**
     * the total number of measurements
     */
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    public void copy(DataPoint dp){
        this.setMean(dp.getMean());
        this.setFirst(dp.getFirst());
        this.setMedian(dp.getMedian());
        this.setLast(dp.getLast());
        this.setCount(dp.getCount());
    }

    @Override
    public boolean equals(Object o) {
        if ((o == null) || (!(o instanceof DataPoint))) {
            return false;
        }
        if (this == o) {
            return true;
        }
        return this.toString().equals(o.toString());
    }

    @JsonIgnore
    public boolean isValid(){
        return type.isValid(this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMean(), getCount(),
                getFirst(), getMedian(), getLast());
    }

    public static class Builder {
        private int first;
        private int mean;
        private int median;
        private int last;
        private int count;
        private DataPointType type;

        public Builder withType(DataPointType type) {
            this.type = type;
            return this;
        }

        public Builder withFirst(int first) {
            this.first = first;
            return this;
        }

        public Builder withMean(int mean) {
            this.mean = mean;
            return this;
        }

        public Builder withMedian(int median) {
            this.median = median;
            return this;
        }

        public Builder withCount(int count) {
            this.count = count;
            return this;
        }

        public Builder withLast(int last) {
            this.last = last;
            return this;
        }

        public DataPoint build() {
            return new DataPoint(this.type, this.first, this.median, this.mean, this.last, this.count);
        }
    }
}
